#ifndef PRESENT_C
#define PRESENT_C

extern void encrypt_present(const uint8_t buffer[8], const uint8_t key[10], uint8_t out[8], const uint8_t random);

extern void seed_PRNG(void);

extern const uint8_t SHUFFLE_SBOX;
extern const uint8_t SHUFFLE_PERMUTATION;
extern const uint8_t SHUFFLE_NONE;

#endif
