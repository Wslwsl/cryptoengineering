/**
 *
 *         present.c
 *
 *         Created by Eduardo Novella and Thom Wiggers on 4/17/14.
 *
 *         Based on C code by Dirk Klose
 *
 *
 *      Key length   = 80 bits
 *      Block length = 64 bits
 *      Rounds       = 31
 *      Sbox         = 4 bits (1 nibble)  F_{2^4}
 *
 */

#include <inttypes.h>
#include "present.h"

#ifdef AVR
//seed prng from eeprom
#include <avr/eeprom.h>
#else
// include time to seed prng
#include <time.h>
#endif

// Number of rounds for PRESENT 80-bit key
#define ROUNDS 32

#ifdef DEBUG
#include <stdio.h>
#endif

#define RANDOM
#ifdef RANDOM
#include <stdlib.h>
#endif

const uint8_t SHUFFLE_SBOX        = 1;
const uint8_t SHUFFLE_PERMUTATION = 2;
const uint8_t SHUFFLE_NONE        = 0;

// Substitution Boxes
const uint8_t Sbox[16]     = {0xc,0x5,0x6,0xb,0x9,0x0,0xa,0xd,0x3,0xe,0xf,0x8,0x4,0x7,0x1,0x2};

// Permutations are calculated on-the-fly

// WATCH OUT==>Little-endian!
// Key length   = 10 bytes = 80 bits
//#define KEY {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}
#define KEY {0x11,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}
uint8_t key[] = KEY;

// KEY = CAFEBABEDEADCODEFABA
//uint8_t key[]   =   {0xBA,0xFA,0xDE,0xC0,0xAD,0xDE,0xBE,0xBA,0xFE,0xCA};

// Plaintext blocks 8 bytes = 64 bits
// STATE = 11000000000000FF
//uint8_t state[] = {0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x11};
uint8_t state[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

void resetKey(const uint8_t mykey[]) {
    uint8_t i;
    for (i = 0; i < 10; i++) {
      key[i] = mykey[i];
    }
};


#ifdef RANDOM
void random_sort(uint8_t list[], int random[], const int n) {
	uint8_t i, j, tmp;
    int tmp2;
    for (i = 1; i < n; i++) {
        j   = i;
        tmp = list[i];
        tmp2 = random[i];
        while (j > 0 && random[j-1] > tmp2) {
            list[j] = list[j-1];
            random[j] = random[j-1];
            j--;
        }
        list[j] = tmp;
        random[j] = tmp2;
    }

}
#endif

//-----------------------------------------------------
// Key Scheduling
//-----------------------------------------------------
void generateRoundKey(const uint8_t round){
    uint8_t tmp1,tmp2;
    uint8_t i;

    if (round == 0) {
        return;
    }

    // key = k79k78.....k0 . The 64-bit round key Ki = k63 k62 . . . k0
    // consists of the 64 leftmost bits of the current contents of key
    //--------------------------------------------------------------------
    tmp1  = key[0];
    tmp2  = key[1];
    for (i=0; i<8;i++)
        key[i] = key[i+2];

    key[8] = tmp1;
    key[9] = tmp2;

    //---------------------------------------------------------------------
    // Shift (the key register is rotated by 61 bit positions to the left)
    //---------------------------------------------------------------------
    tmp1 = key[0] & 7;
    for (i=0;i<9;i++)
        key[i] = key[i] >> 3 | key[i+1] << 5;

    key[9] = key[9] >> 3 | tmp1 << 5;

    //---------------------------------------------------------------------------------
    // S-Box application (the left-most four bits are passed through the present S-box)
    //---------------------------------------------------------------------------------
    key[9] = Sbox[key[9]>>4]<<4 | (key[9] & 0xF);

    //-----------------------------
    //round counter addition
    //-----------------------------
    if((round) % 2 == 1)
        key[1] ^= 128;
    key[2] = ((((round)>>1) ^ (key[2] & 15)) | (key[2] & 240));

}

//-----------------------------------------------------
// Main functions in PRESENT
// addRoundKey && Sboxes && pLayer
//-----------------------------------------------------

void addRoundKey(const uint8_t round){
    // consists of the 64 leftmost bits of the current contents of register Key
    //----------------
    // Shift >>16
    //----------------
    uint8_t k;
    generateRoundKey(round);
    for (k=0; k < 8; k++)
        state[k] ^= key[k+2];
}


uint8_t j[] = {0,1,2,3,4,5,6,7};
void sBoxLayer(uint8_t random){
    uint8_t i;
#ifdef RANDOM
    int r[8];
    if (random & SHUFFLE_SBOX) {
        for (i = 0; i < 8; i++) {
            r[i] = rand();
        }
        random_sort(j, r, 8);
    }

#endif
#ifdef DEBUG
    printf("j =");
#endif
    for ( i=0; i < 8; i++) {
        #ifdef DEBUG
        printf(" %u", j[i]);
        #endif
        state[j[i]] = Sbox[state[j[i]]>>4]<<4 | Sbox[state[j[i]] & 0xF];
    }
#ifdef DEBUG
    printf("\n");
#endif
}

void pLayer(uint8_t random){
    uint8_t i           = 0;
    uint8_t position    = 0;
    uint8_t element_src = 0;
    uint8_t bit_src     = 0;
    uint8_t element_dst = 0;
    uint8_t bit_dst     = 0;
    uint8_t tmp_pLayer[8];
    uint8_t j[64] = { 0,1,2,3,4, 5, 6, 7, 8, 9, 10,
                     11,12,13,14,15,16,17,18,19,20,
                     21,22,23,24,25,26,27,28,29,30,
                     31,32,33,34,35,36,37,38,39,40,
                     41,42,43,44,45,46,47,48,49,50,
                     51,52,53,54,55,56,57,58,59,60,
                     61,62,63};
#ifdef DEBUG
    printf("pLayer()\n");
#endif
#ifdef RANDOM
    int r[64];
    if (random & SHUFFLE_PERMUTATION) {
        for (i=0; i < 64; ++i)
            r[i] = rand();
        random_sort(j, r, 64);
    }
#endif

    for(i=0;i<8;i++)
        tmp_pLayer[i] = 0;

#ifdef DEBUG
    printf("j =");
#endif

    for(i = 0; i < 64; ++i) {
#ifdef DEBUG
        printf(" %02d", j[i]);
#endif

        //---------------------------------
        // Create permutation lookup table
        //---------------------------------
        position = (16*j[i]) % 63;
        if(j[i] == 63)        //exception for bit 63
            position = 63;
        //---------------------------------
        // Permute bit to bit
        //---------------------------------
        element_src = j[i] / 8;
        bit_src     = j[i] % 8;
        element_dst = position / 8;
        bit_dst     = position % 8;
        tmp_pLayer[element_dst] |= ((state[element_src]>>bit_src) & 0x1) << bit_dst;
    }
#ifdef DEBUG
    printf("\n");
#endif

    //------------------
    // Update register
    //------------------
    for(i=0;i<=7;i++){
        state[i]     = tmp_pLayer[i];
    }

}


//-----------------------------------------------------
// Encrypt && decrypt
//-----------------------------------------------------
void encrypt(uint8_t random){
    uint8_t round;
    for ( round=0 ; round < ROUNDS-1 ; round++){
        addRoundKey(round);
        sBoxLayer(random);
        pLayer(random);
    }
    addRoundKey(ROUNDS-1);
#ifdef DEBUG
    for( round = 0; round < 10; round++)
      printf(" %.2X", key[round]);
    printf("\n");
#endif
}

void encrypt_present(const uint8_t buffer[], const uint8_t key[], uint8_t out[], const uint8_t random) {
    uint8_t i;
    for (i = 0; i < 8; i++) {
        state[7-i] = buffer[i];
    }
    resetKey(key);
    encrypt(random);

    for (i = 0; i < 8; i++) {
        out[i] = state[7-i];
    }
}

// Seed the random number generator
void seed_PRNG(void) {
#ifdef AVR
    srand(eeprom_read_word((uint16_t*) 10));
    eeprom_update_word((uint16_t*) 10, (uint16_t) rand());
#else
    srand(time(0));
#endif
}

// vim: sw=4 et :
