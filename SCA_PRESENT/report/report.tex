\documentclass{article}

\usepackage[backend=biber]{biblatex}
\bibliography{report.bib}

\usepackage{bookmark}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{microtype}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage{listings}

\graphicspath{ {./images/} }
\DeclareGraphicsExtensions{.png,.jpg,.pdf}

\newcommand{\present}{\textsc{Present}\xspace}
\newcommand{\dpa}{\textsc{dpa}\xspace}
\newcommand{\Dpa}{\textsc{Dpa}\xspace}
\newcommand{\eeprom}{\textsc{eeprom}\xspace}


\title{Protecting PRESENT with Shuffling Techniques\\ {
\large Cryptographic Engineering}}

\author{Thom Wiggers \& Eduardo Novella Lorente}

\begin{document}
\lstset{language=C}
\maketitle

\section{Introduction}

\present\cite{present} is an ultra lightweight block cipher, included in
a international standard\footnote{ISO/IEC 29192-2:2012}. \present is
a substitution-permutation network (SPN) with 3 main layers: {\tt addRoundKey,
sBoxLayer} (substitutions) and {\tt pLayer} (permutation). It consists of 31
rounds, has a block length of 64 bits and supports key lengths of 80 or 120
bits.

This block cipher is used in low-security applications and constrained
environments, for instance in RFID tags deployments because of a very small
footprint in hardware (around 1550 GE) and its low power estimation ($5\mu$W).

We will briefly discuss the main layers of \present, as shown in
\ref{fig:presentfig}, to better understand the following sections.

\begin{itemize}
	\item {\tt addRoundKey} is a simple XOR operation between the key and the
      state.
	\item {\tt pLayer} is a  state's bit substitution with a table created with
      64 possibilities by calculating ($(16\times \mbox{position}) \mod 63$).
	\item {\tt sBoxLayer} is a nibble to nibble (4 to 4 bits) substitution such that:
\end{itemize}
\begin{center}
\begin{tabular}{| c | c | c | c | c |c | c | c |c | c | c | c | c|c|c|c|c|}
	\hline
x    & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & A & B & C & D & E &F\\
	\hline
S[x] & C & 5 & 6 & B & 9 & 0 & A & D & 3 & E & F & 8 & 4 & 7 & 1 &2\\
	\hline
\end{tabular}
\end{center}

The key scheduling and permutation can be implemented ``on-the-fly'' being very
attractive for small size implementations and constrained environments.

\begin{figure}[hbtp]
  \centering
  \includegraphics[width=1\linewidth]{present}
  \caption{A top level algorithmic description of \present\cite{present}.}
  \label{fig:presentfig}
\end{figure}

For this paper, we chose an implementation of \present with a key length of 80
bits.

\subsection{Random shuffling countermeasures}

S-box lookups, as used in \present, are vulnerable to Differential Power
Analysis (\dpa)~\cite{messerges99}. \Dpa is a statistical method for analysing sets of
measurements to identify data-dependant correlations~\cite{kocher11}.
There are various ways to reduce the leakage from smartcards by using random data,
for instance introducing random dummy operations or executing operations
in a random order~\cite{kocher01}.

In this report, we investigated the gain in resistance to \dpa comparing
a protected and an unprotected implementation of the \present block cipher. We
measured this in term of the amount of traces necessary to recover the key. We
focused on the \dpa resistance gain by introducing a random order of
operations. Different amounts of protection were introduced step by step and we
observed how the leakages were notably reducing.

\section{Implementation of shuffling in PRESENT}

First of all, we implemented our own implementation of \present in order to
quickly discover which operations could be shuffled in a different order. We
identified two layers in each \present round where the order of operations did
not alter the result of the calculations. These were the S-box and
permutation layers. Most of the \present layers can be randomly shuffled with
exception of {\tt addRoundKey} because it depends on the previous state.

Secondly, we had to figure out how to generate entropy inside of the smartcard,
an ATmega163. We discussed several techniques on how to generate entropy in
a secure manner to avoid to use always the same seed. We found different
techniques to extract random numbers in the smartcard. Our first idea was to
combine them to achieve better randomness. After trying to utilise the ADC
(Analogue to Digital Conversion) of the ATmega163, we noticed that of in the
smartcard the pins required for getting ADC measurements were not connected.

Because of time constraints, we then chose to just use the C library
\texttt{rand()} and \texttt{srand()} functions, where we wrote the result of
the \texttt{rand()} call to persistent memory (\eeprom). The next time the
random number generator was seeded, we used this stored random number to seed
\texttt{srand}, and immediately wrote a new random number to memory. This
ensures that random numbers will not repeat because of storing the same seed.

Finally we got several of versions of \present : unprotected (no randomized)
and protected version (randomly shuffled) as countermeasure to side channels
attacks.

\lstset{caption={Random number generator seeding},label=lst:randomseed}
\begin{lstlisting}
// Seed the random number generator,
// executed at card startup
void seed_PRNG(void) {
    srand(eeprom_read_word((uint16_t*) 10));
    eeprom_update_word((uint16_t*) 10, (uint16_t) rand());
}
\end{lstlisting}

Since \eeprom only allows for a finite amount of writes, other methods of
collecting randomness should be explored should one would want to deploy cards.

We used the random values obtained from \verb+rand()+ to build a list $r$ as long
as the amount of iterations a loop had: so for the S-box lookup we built an
list of length 8, for the permutation a list of length 64. We also created
lists $j$ $\{0, \dots n\}$ where $n$ is the amount of iterations. We then
sorted the list $j$ by the values of $r$. Looping over each element of $j$
in the order of $r$ then gave us the index of the byte or bit we should
process that iteration.

The sorting algorithm is explained in appendix \ref{sec:sorting}

\section{Results of implementing the countermeasure}

We loaded our implementation of \present onto an ATmega163 smartcard, setting
the key to \texttt{0xFFFFFFFFFFFFFFFFFFFF}.

We then collected 1000 power traces of our unprotected implementation using
a power tracer and the Inspector software by Riscure. We sampled power traces
at a rate of 125MHz, collecting $1.7$M samples per trace. We also used this
application for all further processing.

After collecting the traces we resampled the traces and aligned them so the
traces mostly overlapped. Aligning significantly increases the effectiveness of
\dpa attacks.\cite{mangard07} The result you can see in figure
\ref{fig:unprotectedtrace}. We could easily identify the 64-times repeated
process of the permutation in every round of \present. The S-box
application happens directly before that. One round takes around
5 milliseconds.

\begin{figure}[hbtp]
  \centering
  \includegraphics[width=\linewidth]{unprotected}
  \caption{Power trace of two rounds of the unprotected implementation of
    \present. The repeating, regular pattern making up most of the trace is
    permutation.}
  \label{fig:unprotectedtrace}
\end{figure}

Using Inspector's First Order Analysis feature we tried to break one nibble of the key.
We were able to get the correct result with a high correlation ($0.9$) after only 20 cycles.
In the first order analysis correlation graph in figure \ref{fig:unprotected-foa},
you can see a very high correlation in the S-box in the graph of the unprotected version.

\begin{small}
\begin{verbatim}
Results after 20 traces
Best correlation Round 1: Key 1:
rank: 1, candidate: 15 (0x0F), confidence: 0.9025 at position: 798
\end{verbatim}
\end{small}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{unprotected}
  \includegraphics[width=\linewidth]{unprotected-foa}
  \caption{Power trace of the unprotected implementation of \present, with
    below it the first order analysis results. Note the peak in the S-box.}
  \label{fig:unprotected-foa}
\end{figure}

Just shuffling the S-boxes and not yet applying shuffling to the permutation
step we were still able to get the key nibble within a 100 cycles (no precise
amount of cycles was researched), although confidence was a bit lower ($0.7$).
These traces we also collected at 125 MHz, while collecting 2M samples per
trace.

\begin{small}
	\begin{verbatim}
rank: 1, candidate: 15 (0x0F), confidence: 0.7895 at position: 8493
	\end{verbatim}
\end{small}

It finds the candidate at a much later position, namely in the permutation. In
figure \ref{fig:protected-sbox-foa} you can see that in the S-box correlation
has been significantly reduced. However, in the permutation there still is
a high correlation, from which the key nibble is recovered.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{only-sbox-protected-trace}
  \includegraphics[width=\linewidth]{only-sbox-protected-foa}
  \caption{Power trace and first order analysis of an implementation
    with a shuffled S-box. Note the peak in the S-box
    disappeared.}
  \label{fig:protected-sbox-foa}
\end{figure}

After implementing shuffling for both the S-box and permutation steps we again
collected 1000 traces, using a sample rate of 125 MHz and taking 4 million
samples per trace. The samples were then first resampled and then aligned at
the position shown in figure \ref{fig:alignment-location}. However,
alignment quickly fell apart as displayed in figure \ref{fig:alignment-zoom}.
This is a consequence of the shuffling, as its implementation of sorting a list
of random values takes a different amount of time depending on the way the
items are sorted. Round length clearly varied per round. When traces don't
overlap much, it becomes much harder to use \dpa.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{protected-alignment-location}
  \caption{Location where alignment was done, at 64 milliseconds}
  \label{fig:alignment-location}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{alignment-zoom}
  \includegraphics[width=\linewidth]{alignment-break}
  \caption{Alignment at the location, and how it broke up a bit further}
  \label{fig:alignment-zoom}
\end{figure}

This showed in first order analysis. It showed a much noisier correlation
graph, as visible in figure \ref{fig:protected-foa}. There was one big peak,
but that only had a correlation of 0.15. First Order Analysis was unable to
calculate the correct nibble from the power trace using 1000 traces.

\begin{small}
	\begin{verbatim}
Results after 1000 traces
Best correlation Round 1: Key 1:
rank: 1, candidate: 13 (0x0D), confidence: 0.1550 at position: 129907
rank: 4, candidate: 15 (0x0F), confidence: 0.1362 at position: 53547
Key entropy: 4
Key info:  xxxx1101/4 bits entropy (0x0d)
	\end{verbatim}
\end{small}

\begin{figure}[hbtp]
  \centering
  \includegraphics[width=\linewidth]{protected-foa}
  \caption{First order analysis on the protected trace shows significantly reduced
    correlation. The peak at 88 ms only reaches a correlation of $0.15$.}
  \label{fig:protected-foa}
\end{figure}

We then mapped the power traces to the frequency domain using Inspector's
spectrum analysis tool, which uses Fast Fourier transformations. When using
this method, alignment of traces is no longer necessary.\cite[page
212]{mangard07} The obtained graph is shown in figure
\ref{fig:protected-spectrum}. When applying first order analysis, this however
also failed to obtain the key.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=\linewidth]{protected-spectrum}
  \includegraphics[width=\linewidth]{protected-spectrum-foa}
  \caption{Spectral analysis of the protected implementation. First order analysis
    was here also unsuccessful with 1000 traces.}
  \label{fig:protected-spectrum}
\end{figure}

We thus got an increase in \dpa resistance of at least 50 $\times$ the amount
of cycles needed to obtain the correct key when comparing the fully protected
and unprotected versions. However, performing the extra operations needed to
shuffle the order of operations comes with a significant cost. Whilst one
complete application of \present costs roughly $0.16$ seconds in the
unprotected version, it takes around 1 second in the fully protected version.
This means that the unprotected version is roughly $6.4$ times faster than the
fully protected version, which has both the S-box and permutation done
in random order.

Part of this slowdown can be mitigated by choosing a better shuffling algorithm
to select the order in which the operations will take place: we used a version
with $O(n^2)$ time complexity. $O(n \log n)$ implementations are possible,
which would make the shuffling of the permutation layer significantly shorter.
A more than two times slowdown is still to be expected, however.

\section{Conclusions}

We realised that side channel attacks are quite hard to defend against. What
looks ``safe'' and correctly implemented can leak your key data in a matter of
seconds, just because an attacker measured some power traces.

We also appreciated that the experience helps a lot to defend against side
channel attacks and we were quite novices. Although one of us had had some
lectures with Inspector during the course \emph{Hardware Security}, these lab
sessions have been definitely much more straightforward because we implemented
our own code and tested it against \dpa. It has been interesting watch how
different leakages can appear and disappear with only small changes in the
source code.

As any kind of side channel can reveal too much secret information, we know how
to use software techniques as randomising operations, masking, dummy operations
and constant time protections to significantly increase the difficulty of side
channel attacks and to make it as hard as possible for an attacker.

\present is a good candidate for using randomisation countermeasures, but it
does come with a significant cost to runtime performance. While our
implementation of shuffling was not the most efficient one, a performance cost
factor of two seems the very minimum. The ease with which the key could be
recovered without the countermeasures, though, makes implementing shuffling
almost mandatory. Optimising the shuffling approach therefore seems like a good
area for further improvement.

\printbibliography

\appendix
\section{Sorting algorithm} \label{sec:sorting}

We used a modified version of insertion sort to operate on two lists.

\lstset{label=lst:sorting, caption={Insertion sort on two lists}}
\begin{lstlisting}
// list[] is a list of {0 .. n}
// random is a list of length n containing random values
void random_sort(uint8_t list[], int random[], const int n) {
    uint8_t i, j, // list iterator indices
            tmp;  // temp for current list[] item
    int     tmp2; // temp for current random[] item
    for (i = 1; i < n; i++) {
        j   = i;
        tmp = list[i];
        tmp2 = random[i];
        while (j > 0 && random[j-1] > tmp2) {
            list[j] = list[j-1];
            random[j] = random[j-1];
            j--;
        }
        list[j] = tmp;
        random[j] = tmp2;
    }
}
\end{lstlisting}


\end{document}
% vim: set ts=4 sw=2 tw=0 et :
