Ideas for (pseudo)random number generation in a MicroController ATmega163
===========================================================================

* Write seed to EEPROM, increment (other operation?) on use and write back
  the new seed.
* Use Watchdog timer to periodically change state of RNG, so that when waiting
  for input we sometimes change the RNG?
    * Watchdog timer does completely reset....
* Use other interrupts?
* Counter, timer?


Links
==

* Techniques:  entropy buffer + watchdog timer (oscillator) +  XORshifts + EEPROM's write operations timing (check if our MC can do it)

+ Watchdog timer: https://gist.github.com/endolith/2568571
+ Entropy gathering for cryptographic applications on AVR: http://n1.taur.dk/avrfreaks/engather.pdf
+ avr-hardware-random-number-generation  https://code.google.com/p/avr-hardware-random-number-generation/


* Some useful comments:

+ The watchdog uses its own RC oscillator, and has phase noise relative to the main clocksource.
Set up a timer so it counts every clock. An 8 bit timer is plenty. In the watchdog interrupt read the timer, and hash the value. 
I usually do so for ~4s before claiming to have 128 bits of entropy to drive cryptographic functions. 

+ http://stackoverflow.com/questions/10864668/is-it-possible-to-generate-random-numbers-using-physical-sensors

* Timing
+ "Most AVRs have internal EEPROM memory. Write operations to this memory are timed by a dedicated timer which is independent of the main system clock 
(edit: note that in some AVRs, ATTiny25/45/85 for example, the EEPROM timing is derived from the internal RC oscillator, so that no entropy can be gathered when this oscillator is also selected as the system clock source); this may depend on the main clock source (internal R/C vs. external crystal/resonator). 
Therefore, there is some (truly random) jitter to be expected in the time it takes to write to the EEPROM with respect to the main system clock, which again can be measured be a high-speed timer/counter."

+ "Newer AVRs have the capability to let the watchdog timer generate a software interrupt instead of a hardware reset. The watchdog timer is by design controlled by its own independent clock source, which yields the relative jitter one can measure."

+ Many AVRs have the capability to have a dedicated timer/counter be clocked from an external 32kHz crystal for improved accuracy of real-time measurements. This external crystal is another source of events uncorrelated with the main clock. (Otherwise there would be no use in the extra crystal in the first place...)

+ Timing the EEPROM write produced about 4.82 bits of entropy per write operation, while the watchdog timer seems more stable frequency-wise yielding about 3.92 bits per watchdog timeout. Additionally, the EEPROM's write times seem much more smoothly Gauss-distributed where the WDT's distribution seems somewhat asymmetric and with a lot of aberrations. 

