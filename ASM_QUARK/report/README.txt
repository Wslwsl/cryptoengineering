
Some important notes regarding Assignment 1:

    The deadline is set for Friday 28th of March (until 12 o’clock at night)
    The core deliverable of the assignment is the code itself
    The cipher test vectors must be working correctly
    The implementation must be time constant, i.e. run in a constant number of cycles
    You do not need to perform IO operations, you can start with the plaintext/key being in SRAM or registers
    You only have to implement one variant of the cipher or hash function
    In the end you will deliver a 4-page report that briefly describes the cipher, explains the exact ideas you used in order to make it smaller or faster and it will conclude with an array that contains size (bytes in SRAM and Flash) and speed (in clock cycles).
    There will also be meetings where you will explain in detail what your code does and you will justify exactly what you did to make it smaller or faster.
    Mail the code and the report to kostaspap88@gmail.com

