 /** Cycles count:
 CLR  5
 AND  8
 XOR 19
 BST 13
 BLD 18
 LDI  3
 MOV  3
 -------
    -------------------->              69 #clocks
 3 X linear_equals   (13/14)* 3     39/42 #clocks


 +---------------------------------------------+
 |TOTAL #CYCLES :        108/111 #clocks       |
 +---------------------------------------------+
 */

.macro hmacro
; now bit N is in position N-1
; bit 0 is in Y0[4]
CLR T0
CLR T1
CLR T2
CLR T3
CLR T4 ; used for storage of result H

; XOR with X[1]
BST X0, 3
BLD T4, 0

; XOR with Y[2]
BST Y0, 2
BLD T0, 0
EOR T4, T0

; XOR with Y[10]
BST Y1, 2
BLD T0, 0
EOR T4, T0

; XOR with X[31]
BST X4, 5
BLD T0, 0
EOR T4, T0

; XOR with Y[43]
BST Y5, 1
BLD T0, 0
EOR T4, T0

; XOR with X[56]
BST X7, 4
BLD T0, 0
EOR T4, T0

; XOR with X[4]
BST X0, 0
BLD T0, 0
EOR T4, T0

; XOR with L[0]
BST L0, 1
BLD T0, 0
EOR T4, T0

; XOR with X[25]
BST X3, 3
BLD T1, 0
EOR T4, T1
AND T1, T0 ; L[0] AND X[3]
EOR T4, T1
BLD T0, 5

; T0 now stores temp results
; bit |  value
; 0   |  L[0]
; 1   |  Y[59]
; 2   |  Y[3]
; 3   |  X[55]
; 4   |  X[46]
; 5   |  X[25]

; XOR with Y[59]
BST Y7, 1
BLD T0, 1 ; store for later
BLD T3, 0
EOR T4, T3 ; keep T3 around for a bit.

; XOR X[55] & Y[59]

; load Y[3]
BST Y0, 1
BLD T1, 0
BLD T0, 2

;load X[55]
BST X7, 5
BLD T0, 3  ; store for later
BLD T2, 0
AND T1, T2 ; Y[3] & X[55]
AND T3, T2 ; Y[59] & X[55]
EOR T4, T1 ; XOR Y[3] & X[55]
EOR T4, T3 ; XOR X[55] & Y[59]

; load X46
BST X6, 6
BLD T3, 0
BLD T0, 4
AND T1, T3 ; Y[3] & X[55] & X[46]
EOR T4, T1
AND T3, T2 ; X[55] & X[46]
EOR T4, T3

; result ^= Y[3] & X[25] & X[46];
; bit        2      5       4
LDI T1, ((1<<2)+(1<<5)+(1<<4))
MOV T2, T1
AND T1, T0
linear_equals T1, T2 ; 13 cycles... might be possible to do faster if just done without AND
EOR T4, T1

; result ^= Y[3] & X[46] & Y[59];
; bit        2      4       1
LDI T1, ((1<<2)+(1<<4)+(1<<1))
MOV T2, T1
AND T1, T0
linear_equals T1, T2 ; might be faster if done by just bld/bst or saving prev. results
EOR T4, T1

; result ^= L[0] & X[25] & X[46] & Y[59];
; bit        0      5       4       1
LDI T1, ((1<<0)+(1<<5)+(1<<4)+(1<<1))
MOV T2, T1
AND T1, T0
linear_equals T1, T2
EOR T4, T1

; Feed result back into X, Y
EOR Y8, T4
EOR X8, T4
; end

.endm