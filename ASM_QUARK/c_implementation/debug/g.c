#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define TIMES 50
uint8_t Y[68];
uint8_t state[17*8]; // one uint8_t per bit.
uint8_t iv[] = {0xd8,0xda,0xca,0x44,0x41,0x4a,0x09,0x97,
      		0x09,0xc8,0x0a,0xa3,0xaf,0x06,0x56,0x44,0xdb};  //09 instead 19

uint8_t g1(uint8_t * Y) {
	  uint8_t result = Y[0];
	printf("Y[0]                                           => %X =%d\n",Y[0],result);

	  result ^= Y[16];
	printf("Y[16]                                          => %X =%d\n",Y[16],result);

	  result ^= Y[49];
	printf("Y[49]                                          => %X =%d\n",Y[49],result);

	  result ^= Y[7];
	printf("Y[7]                                           => %X =%d\n",Y[7],result);

	  result ^= Y[7] & Y[15];
	printf("Y[7] & Y[15]                                   => %X %X =%d\n",Y[7], Y[15],result);
	  
	  result ^= Y[20];
	printf("Y[20]                                          => %X =%d\n",Y[20],result);

	  result ^= Y[30];
	printf("Y[30]                                          => %X =%d\n",Y[30],result);

	  result ^= Y[35];
	printf("Y[35]                                          => %X =%d\n",Y[35],result);

	  result ^= Y[37];
	printf("Y[37]                                          => %X =%d\n",Y[37],result);

	  result ^= Y[42];
	printf("Y[42]                                          => %X =%d\n",Y[42],result);

	  result ^= Y[51];
	printf("Y[51]                                          => %X =%d\n",Y[51],result);

	  result ^= Y[54];
	printf("Y[54]                                          => %X =%d\n",Y[54],result);

	  result ^= Y[54] & Y[58];
	printf("Y[54] & Y[58]                                  => %X %X =%d\n",Y[54],Y[58],result);

	  result ^= Y[35] & Y[37];
	printf("Y[35] & Y[37]                                  => %X %X =%d\n",Y[35],Y[37],result);

	  result ^= Y[42] & Y[51] & Y[54];
	printf("Y[42] & Y[51] & Y[54]                          => %X %X %X =%d\n",Y[42],Y[51],Y[54],result);

	  result ^= Y[20] & Y[30] & Y[35];
	printf("Y[20] & Y[30] & Y[35]                          => %X %X %X =%d\n",Y[20] ,Y[30] ,Y[35],result);

	  result ^= Y[7] & Y[30] & Y[42] & Y[58];
	printf("Y[7] & Y[30] & Y[42] & Y[58]                   => %X %X %X %X =%d\n",Y[7] , Y[30] , Y[42] , Y[58],result );

	  result ^= Y[35] & Y[37] & Y[51] & Y[54];
	printf("Y[35] & Y[37] & Y[51] & Y[54]                  => %X %X %X %X =%d\n",Y[35] , Y[37] ,Y[51], Y[54] ,result );

	  result ^= Y[15] & Y[20] & Y[54] & Y[58];
	printf("Y[15] & Y[20] & Y[54] & Y[58]                  => %X %X %X %X =%d\n",Y[15] ,Y[20] , Y[54] , Y[58] ,result );

	  result ^= Y[37] & Y[42] & Y[51] & Y[54] & Y[58];
	printf("Y[37] & Y[42] & Y[51] & Y[54] & Y[58]          => %X %X %X %X %X =%d\n",Y[37] ,Y[42] ,Y[51] , Y[54] , Y[58],result);

	  result ^= Y[7] & Y[15] & Y[20] & Y[30] & Y[35];
	printf("Y[7] & Y[15] & Y[20] & Y[30] & Y[35]           => %X %X %X %X %X =%d\n",Y[7] , Y[15] , Y[20] ,Y[30], Y[35] ,result);

	  result ^= Y[20] & Y[30] & Y[35] & Y[37] & Y[42] & Y[51];
	printf("Y[20] & Y[30] & Y[35] & Y[37] & Y[42] & Y[51]  => %X %X %X %X %X %X =%d\n",Y[20],Y[30],Y[35],Y[37],Y[42] , Y[51] ,result);

	  return result;
}

uint8_t g(uint8_t * Y) {
	  uint8_t result = Y[0];
	printf("result = Y[0]                                           => %X\n",result);

	  result ^= Y[16];
	printf("result ^= Y[16]                                         => %X\n",result);

	  result ^= Y[49];
	printf("result ^= Y[49]                                         => %X\n",result);

	  result ^= Y[7];
	printf("result ^= Y[7]                                          => %X\n",result);

	  result ^= Y[7] & Y[15];
	printf("result ^= Y[7] & Y[15]                                  => %X\n",result);
	  
	  result ^= Y[20];
	printf("result ^= Y[20]                                         => %X\n",result);

	  result ^= Y[30];
	printf("result ^= Y[30]                                         => %X\n",result);

	  result ^= Y[35];
	printf("result ^= Y[35]                                         => %X\n",result);

	  result ^= Y[37];
	printf("result ^= Y[37]                                         => %X\n",result);

	  result ^= Y[42];
	printf("result ^= Y[42]                                         => %X\n",result);

	  result ^= Y[51];
	printf("result ^= Y[51]                                         => %X\n",result);

	  result ^= Y[54];
	printf("result ^= Y[54]                                         => %X\n",result);

	  result ^= Y[54] & Y[58];
	printf("result ^= Y[54] & Y[58]                                 => %X\n",result);

	  result ^= Y[35] & Y[37];
	printf("result ^= Y[35] & Y[37]                                 => %X\n",result);

	  result ^= Y[42] & Y[51] & Y[54];
	printf("result ^= Y[42] & Y[51] & Y[54]                         => %X\n",result);

	  result ^= Y[20] & Y[30] & Y[35];
	printf("result ^= Y[20] & Y[30] & Y[35]                         => %X\n",result);

	  result ^= Y[7] & Y[30] & Y[42] & Y[58];
	printf("result ^= Y[7] & Y[30] & Y[42] & Y[58]                  => %X\n",result);

	  result ^= Y[35] & Y[37] & Y[51] & Y[54];
	printf("result ^= Y[35] & Y[37] & Y[51] & Y[54]                 => %X\n",result);

	  result ^= Y[15] & Y[20] & Y[54] & Y[58];
	printf("result ^= Y[15] & Y[20] & Y[54] & Y[58]                 => %X\n",result);

	  result ^= Y[37] & Y[42] & Y[51] & Y[54] & Y[58];
	printf("result ^= Y[37] & Y[42] & Y[51] & Y[54] & Y[58]         => %X\n",result);

	  result ^= Y[7] & Y[15] & Y[20] & Y[30] & Y[35];
	printf("result ^= Y[7] & Y[15] & Y[20] & Y[30] & Y[35]          => %X\n",result);

	  result ^= Y[20] & Y[30] & Y[35] & Y[37] & Y[42] & Y[51];
	printf("result ^= Y[20] & Y[30] & Y[35] & Y[37] & Y[42] & Y[51] => %X\n",result);

	  return result;
}


void showstate() {
  uint8_t buffer = 0;
  for (int i = 0; i < 8*17; ++i) {
    buffer ^= (1&state[i]) << (7-(i%8));
    if (((i%8)==7) && i) {
      printf("%02x", buffer);
      buffer = 0;
    }
  }
  printf("\n");
}

int main(void) {

	// Load IV into state
	for (int i = 0; i < 17 * 8; i++) {
		uint8_t word = iv[i/8];
		uint8_t bit = word >> (7-(i%8)) & 1;
		state[i] = bit;
	}

	// Load Y from state[68:]
  	for(int i = 0; i < 68; i++) 
		Y[i] = state[i+68];
		

	// Just a some times to check it out
	for ( int i=0; i <TIMES ; i++){
		printf("\n\ni=%d\n",i);
		uint8_t result = g1(Y);
		
		// Clock  Y
    		for(int j = 0; j < 68-1; j++) 
		      Y[j] = Y[j+1];
		Y[67] = result; 

 		// copy Y back into state
  		for(int j = 0; j < 68; j++) 
    	    		state[j+68] = Y[j];
		showstate();
	}

	return 0;
}

