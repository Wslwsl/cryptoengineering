#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>

#define DEBUG
#define DEBUG_PERMUTE
#define CAPACITY 16 // capacity c in bytes
#define RATE 1   // rate r in bytes
#define WIDTH 17 // length b=r+c, output in bytes.

void showstate();

/* 17 bytes */
uint8_t iv[] = {0xd8,0xda,0xca,0x44,0x41,0x4a,0x09,0x97,
                0x19,0xc8,0x0a,0xa3,0xaf,0x06,0x56,0x44,0xdb};

uint8_t state[WIDTH*8]; // one uint8_t per bit.
uint8_t out[WIDTH];
//input in bits.
#define LENGTH 0
uint8_t message[8*(LENGTH+RATE)] = {};
int length = LENGTH;

/*
 * initialisation step: load IV into state
 */
void init() {
  // copy iv into state
  for (int i = 0; i < WIDTH * 8; i++) {
    // grab the floor(i/8)th byte from the test vector.
    uint8_t word = iv[i/8];
    // grab the bit we want, so first the last one, then the second one, etc.
    uint8_t bit = word >> (7-(i%8)) & 1;

    state[i] = bit;
  }
}

uint8_t p(uint8_t * L) {
  return L[0] ^ L[3];
}

// todo replace by several registers of actual width.
uint8_t h(uint8_t * X, uint8_t * Y, uint8_t* L) {
  uint8_t result = L[0];
  result ^= X[1];
  result ^= Y[2];
  result ^= X[4];
  result ^= Y[10];
  result ^= X[25];
  result ^= X[31];
  result ^= Y[43];
  result ^= X[56];
  result ^= Y[59];
  result ^= Y[3] & X[55];
  result ^= X[46] & X[55];
  result ^= X[55] & Y[59];
  result ^= Y[3] & X[25] & X[46];
  result ^= Y[3] & X[46] & X[55];
  result ^= Y[3] & X[46] & Y[59];
  result ^= L[0] & X[25] & X[46] & Y[59];
  result ^= L[0] & X[25];
  return result;
}

uint8_t f(uint8_t * X) {   //implemented
  uint8_t result = X[0];   // x
  result ^= X[9];          // x
  result ^= X[14];         // x
  result ^= X[21];         // x
  result ^= X[28];         // x
  result ^= X[33];         // x
  result ^= X[37];         // x
  result ^= X[45];         // x
  result ^= X[50];         // x
  result ^= X[52];         // x
  result ^= X[55];         // x
  result ^= X[55] & X[59]; // x
  result ^= X[33] & X[37]; // x
  result ^= X[9] & X[15];  // x
  result ^= X[45] & X[52] & X[55]; // x
  result ^= X[21] & X[28] & X[33]; // x
  result ^= X[9] & X[28] & X[45] & X[59]; // x
  result ^= X[33] & X[37] & X[52] & X[55]; // x
  result ^= X[15] & X[21] & X[55] & X[59]; // x
  result ^= X[37] & X[45] & X[52] & X[55] & X[59]; // x
  result ^= X[9] & X[15] & X[21] & X[28] & X[33]; // x
  result ^= X[21] & X[28] & X[33] & X[37] & X[45] & X[52]; // x
  return result;
}

uint8_t g(uint8_t * Y) {
  uint8_t result = Y[0];
  result ^= Y[7];
  result ^= Y[16];
  result ^= Y[20];
  result ^= Y[30];
  result ^= Y[35];
  result ^= Y[37];
  result ^= Y[42];
  result ^= Y[49];
  result ^= Y[51];
  result ^= Y[54];
  result ^= Y[54] & Y[58];
  result ^= Y[35] & Y[37];
  result ^= Y[7] & Y[15];
  result ^= Y[42] & Y[51] & Y[54];
  result ^= Y[20] & Y[30] & Y[35];
  result ^= Y[7] & Y[30] & Y[42] & Y[58];
  result ^= Y[35] & Y[37] & Y[51] & Y[54];
  result ^= Y[15] & Y[20] & Y[54] & Y[58];
  result ^= Y[37] & Y[42] & Y[51] & Y[54] & Y[58];
  result ^= Y[7] & Y[15] & Y[20] & Y[30] & Y[35];
  result ^= Y[20] & Y[30] & Y[35] & Y[37] & Y[42] & Y[51];
  return result;
}
/**
 * Perform the permutations.
 */
void permute() {
  // length = log2(4*b)
  // this is the LFSR
  uint8_t L[10] = {1,1,1,1,1,1,1,1,1,1};
  // 68+68 = 136 = 8 bits/byte * 17 bytes
  // these are the NFSRs
  //actually, this should just be happening inside
  //of state. We can't parallelize anyway. TODO
  uint8_t X[68], Y[68];

  // copy
  for(int i = 0; i < 68; i++) {
    X[i] = state[i];
    Y[i] = state[i+68];
  }

  for (int i = 0; i < 544; i++) {
    // implement: f, g, h, p.

    uint8_t ht = h(X, Y, L);
    uint8_t ft = f(X);
    uint8_t gt = g(Y);
    uint8_t pt = p(L);

    // clock registers X and Y:
    uint8_t Y0 = Y[0];
#ifdef DEBUG
    uint8_t Y1 = Y[1], X1=X[1];
#endif
    for(int j = 0; j < 68-1; j++) {
      X[j] = X[j+1];
      Y[j] = Y[j+1];
    }

#ifdef DEBUG
    assert(Y1 == Y[0]);
    assert(X1 == X[0]);
    assert(Y[67] == Y[66]);
    assert(X[66] == X[67]);
#endif

    // set last bit:
    X[67] = Y0 ^ ft ^ ht;
    Y[67] = gt ^ ht;

    // clock L.
    for (int j = 0; j < 9; j++) {
      L[j] = L[j+1];
    }
    L[9] = pt; // L[0] XOR L[3]

#ifdef DEBUG_PERMUTE
    printf("\nh = %u", ht);
    printf("\ng = %u", gt);
    printf("\nL = ");
    for(int j = 0; j < 10; j++) {
      printf("%u", L[j]);
    }
    printf("\nX = ");

    for(int j = 0; j<68; ++j) {
      printf("%u", X[j]);
    }
    printf("\nY = ");
    for(int j = 0; j < 68; ++j) {
      printf("%u", Y[j]);
    }
    printf("\n");

    // copy back into state
    for(int j = 0; j < 68; j++) {
      state[j] = X[j];
      state[j+68] = Y[j];
    }

    showstate();
#endif

  }
  // copy back into state
  for(int j = 0; j < 68; j++) {
    state[j] = X[j];
    state[j+68] = Y[j];
  }

}

/**
 * Pad the input.
 */
void pad() {
  // append 1
  message[LENGTH] = 1;

  int padding = (RATE*8 - (LENGTH+1)%(RATE*8))%8;

  // this can probably be made an XOR
  for(int i = 1; i <= padding; i++) {
#ifdef DEBUG
    printf("Pad i=%d\n", i);
#endif
    message[LENGTH+i] = 0;
  }

  length += padding + 1;
#ifdef DEBUG
  printf("Padded: ");
  for (int i= 0; i < length; i++) {
    printf("%x", message[i]);
  }
  printf("\n");
#endif

}

void absorb() {

  printf("Absorbing: \n");
#ifdef DEBUG
  printf("Initial state:  ");
  showstate();
#endif

  for(int i = 0; i < (LENGTH+(RATE*8))/8; i++) {
    for (int j = 0; j < RATE*8; j++) {
      state[(WIDTH-RATE)*8+j] ^= message[j];
    }

#ifdef DEBUG
    printf("XORed state:    ");
    showstate();
    permute();
    printf("Permuted state: ");
    showstate();
#else
    permute();
#endif
  }
}

void squeeze() {

  printf("Squeezing\n");

  for (int i = 0; i < WIDTH;) {  
    out[i] = 0;
    for (int j = 0; j < 8*RATE; j++) {
      out[i] ^= state[8*(WIDTH-RATE)+j] << (7-j);
    }
#ifdef DEBUG
    printf("extracted byte %02x (%d)\n", out[i], i);
#endif

    if (++i < WIDTH) { // waste to do it a last time.
      permute();
#ifdef DEBUG
      printf("Squeeze cycle %02d:", i);
      showstate();
#endif
    }
  }

}

// copied from reference quark.c with minor changes
/**
 * Prints state
 */
void showstate() {
  uint8_t buffer = 0;
  for (int i = 0; i < 8*WIDTH; ++i) {
    buffer ^= (1&state[i]) << (7-(i%8));
    if (((i%8)==7) && i) {
      printf("%02x", buffer);
      buffer = 0;
    }
  }
  printf("\n");
}


int main(void) {
  init();
  pad();
  absorb();
  squeeze();
  printf("output: ");
  for (int i = 0; i < WIDTH; i++) {
    printf("%02x", out[i]);
  }
  printf("\n");

  return 0;

#ifdef DEBUG
  printf("Initialized state with IV\niv:    ");
  for (int i = 0; i < WIDTH; i++) {
    printf("%02x", iv[i]);
  }
  printf("\nstate: ");
  showstate();

  printf("starting permute test\n");
  permute();

  printf("Done test permute\n");
  printf("State:    ");
  showstate();
  printf("expected: 70dda420b4a92205deb7d576eaee8b32b5\n");
#endif
  return 0;
}
